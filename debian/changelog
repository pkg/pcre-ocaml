pcre-ocaml (7.4.6-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 23:09:42 +0000

pcre-ocaml (7.4.6-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper compat level to 13

 -- Stéphane Glondu <glondu@debian.org>  Thu, 20 Aug 2020 08:32:24 +0200

pcre-ocaml (7.4.3-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch
  * Update Vcs-*
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Wed, 29 Jan 2020 09:23:17 +0100

pcre-ocaml (7.2.3-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:24:20 +0000

pcre-ocaml (7.2.3-2) unstable; urgency=medium

  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sat, 15 Jul 2017 11:55:07 +0200

pcre-ocaml (7.2.3-1) unstable; urgency=medium

  * New upstream release
  * Update debian/watch
  * Update Vcs-*
  * Bump Standards-Version to 3.9.8
  * Bump debhelper compat to 9

 -- Stéphane Glondu <glondu@debian.org>  Wed, 03 Aug 2016 13:56:58 +0200

pcre-ocaml (7.0.4-2) unstable; urgency=medium

  * Team upload.
  * Fix homepage URL.

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 10 Aug 2015 20:22:12 +0000

pcre-ocaml (7.0.4-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Stéphane Glondu <glondu@debian.org>  Mon, 27 Jan 2014 10:24:50 +0100

pcre-ocaml (7.0.2-4) unstable; urgency=low

  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:40:37 +0100

pcre-ocaml (7.0.2-3) experimental; urgency=low

  * Compile with OCaml >= 4
  * Update Homepage and Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Thu, 11 Jul 2013 15:50:38 +0200

pcre-ocaml (7.0.2-2) unstable; urgency=low

  * Remove META from -dev (it is installed by -ocaml)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 09 May 2013 08:19:33 +0200

pcre-ocaml (7.0.2-1) unstable; urgency=low

  * New upstream release
  * Do no longer use CDBS
  * Use format version 1.0 in debian/copyright
  * Bump Standards-Version to 3.9.4

 -- Stéphane Glondu <glondu@debian.org>  Wed, 08 May 2013 20:19:11 +0200

pcre-ocaml (6.2.5-1) unstable; urgency=low

  * New upstream release
    - drop Fix-typo-in-debug-native-code-library-target-of-OCam.patch

 -- Stéphane Glondu <glondu@debian.org>  Fri, 10 Feb 2012 06:50:22 +0100

pcre-ocaml (6.2.4-2) unstable; urgency=low

  * Fix linking options stored in OCaml libraries that were making (some)
    reverse-dependencies FTBFS

 -- Stéphane Glondu <glondu@debian.org>  Mon, 02 Jan 2012 13:59:13 +0100

pcre-ocaml (6.2.4-1) unstable; urgency=low

  * New upstream release
    - drop Fix-version-number-in-META.patch (fixed upstream)
    - add Fix-typo-in-debug-native-code-library-target-of-OCam.patch
  * Compile with debugging symbols (Closes: #649863)

 -- Stéphane Glondu <glondu@debian.org>  Sun, 01 Jan 2012 16:17:04 +0100

pcre-ocaml (6.2.3-1) unstable; urgency=low

  * New upstream release
    - drop Change-position-of-flag-lpcre.patch since it seems to have
      been addressed upstream
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 19 Nov 2011 15:27:40 +0100

pcre-ocaml (6.2.2-2) unstable; urgency=low

  * Remove obsolete README.source
  * Update libpcre-ocaml-dev.README.Debian (Closes: #622233)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 18 Apr 2011 11:52:32 +0200

pcre-ocaml (6.2.2-1) unstable; urgency=low

  * New upstream release
    - add Fix-version-number-in-META.patch
  * Add Change-position-of-flag-lpcre.patch
  * debian/control:
    - fix typo in package description (Closes: #590973)
    - remove quilt from Build-Depends
    - remove Stefano from Uploaders
    - bump Standards-Version to 3.9.1
  * Bump debhelper compat level to 8

 -- Stéphane Glondu <glondu@debian.org>  Sun, 20 Feb 2011 05:35:01 +0100

pcre-ocaml (6.0.1-3) unstable; urgency=low

  [ Stéphane Glondu ]
  * Fix generation of documentation by dh_ocamldoc (Closes: #570717)

  [ Stefano Zacchiroli ]
  * Bump Standards-Version to 3.8.4 (no changes)
  * debian/source/format: set source format to 3.0 (quilt)
  * switch patch management from dpatch to quilt
  * debian/patches: remove unused meta.dpatch

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 08 Apr 2010 21:50:07 +0200

pcre-ocaml (6.0.1-2) unstable; urgency=low

  * Switch packaging to dh-ocaml 0.9
  * Update my e-mail address and remove DMUA
  * Bump Standards-Version to 3.8.3 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Mon, 05 Oct 2009 10:26:24 +0200

pcre-ocaml (6.0.1-1) unstable; urgency=low

  * New Upstream Version

 -- Stephane Glondu <steph@glondu.net>  Wed, 01 Jul 2009 00:37:52 +0200

pcre-ocaml (6.0.0-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * debian/*install.in: uniform files, removing spurious dirts
  * debian/control:
    - set archive section to "ocaml"

  [ Stephane Glondu ]
  * New upstream release:
    - changed API wrt. error handling
  * Update Standards-Version to 3.8.2
  * Add versioned dependency to ocaml-findlib to ease OCaml 3.11.1
    transition

 -- Stephane Glondu <steph@glondu.net>  Wed, 01 Jul 2009 00:26:22 +0200

pcre-ocaml (5.15.1-2) unstable; urgency=low

  * upload to unstable
  * bump debhelper compatibility level to 7
    - simplify debian/*.install file getting rid of debian/tmp/
  * debian/*.install.in: use @OCamlStdlibDir@ and @OCamlDllDir@ instead of
    making assumption on stdlib directory layout
  * debian/rules: use ocaml.mk as a "rules" makefile snippet, bump
    build-dep on dh-ocaml to the minumum version implementing it
  * debian/control:
    - drop obsolete dep version requirements
    - version build-deps on findlib, to ensure buildability in unstable

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 25 Feb 2009 11:42:52 +0100

pcre-ocaml (5.15.1-1) experimental; urgency=low

  * New upstream release
  * Update debian/watch
  * Add debian/README.source
  * Update Standards-Version to 3.8.0
  * Update versioned dependency to OCaml

 -- Stephane Glondu <steph@glondu.net>  Fri, 05 Dec 2008 20:02:11 +0100

pcre-ocaml (5.15.0-1) experimental; urgency=low

  [ Stephane Glondu ]
  * New upstream release.
  * Add myself to Uploaders, and DM-Upload-Allowed field.

  [ Stefano Zacchiroli ]
  * change Vcs-* fields to point to the new git repository

  [ Romain Beauxis ]
  * Prepared upload to experimental to build against ocaml 3.11
  * Versioned ocaml-related build-dep to avoid confusion
  * Added dh-ocaml to build-deps

 -- Romain Beauxis <toots@rastageeks.org>  Sun, 30 Nov 2008 06:36:25 +0100

pcre-ocaml (5.13.0-1.1) experimental; urgency=low

  * Rebuild with ocaml 3.10.2, NMU with maintainer's blessing.

 -- Ralf Treinen <treinen@debian.org>  Sat, 22 Mar 2008 21:05:44 +0100

pcre-ocaml (5.13.0-1) unstable; urgency=low

  * new upstream release
  * fix vcs-svn field to point just above the debian/ dir
  * rebuild against ocaml 3.10.1

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 09 Feb 2008 17:58:20 +0100

pcre-ocaml (5.12.2-3) unstable; urgency=low

  * add Homepage field to debian/control
  * update standards-version, no changes needed
  * setting me as an uploader, d-o-m as the maintainer
  * debian/patches: added patch descriptions for build.dpatch and fpic.dpatch
  * debian/rules: remove unneeded win32 .bat file (make_win32_cloc.bat), it
    was useless and its removal makes lintian happy

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 29 Dec 2007 22:32:54 +0100

pcre-ocaml (5.12.2-2) unstable; urgency=low

  * debian/control
    - bump build-dep on ocaml-nox to 3.10.0-8 to ensure we build against fixed
      CDBS class wrt ocamldoc generation

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 02 Sep 2007 23:19:49 +0200

pcre-ocaml (5.12.2-1) unstable; urgency=low

  * new upstream release
  * bump debhelper dep and compatibility level to 5
  * debian/rules
    - use CDBS class support for generating the API reference via ocamldoc
      instead of doing that by hand. Remove doc-base entry for the HTML API
      reference; this is a bit of a regression, but in the future the doc-base
      entries will be automatically generated by CDBS
  * debian/control
    - bump dep on ocaml-nox to 3.10.0-7, since we now use latest stuff of the
      CDBS class
  * debian/patches/build.dpatch
    - ensure DESTDIR is created before attempting to install there with
      findlib (apparently needed starting from this upstream version)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 02 Sep 2007 16:39:34 +0200

pcre-ocaml (5.11.4-1) experimental; urgency=low

  * new upstream release
  * rebuilt against OCaml 3.10.0
  * debian/watch
    - added watch file
  * debian/control
    - use binary:Version instead of Source-Version for strict dependencies
  * debian/patches/*
    - removed heading XX_ in dpatches: 00list is enough for the ordering

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 05 Jul 2007 10:20:08 +0200

pcre-ocaml (5.11.1-2) unstable; urgency=low

  * debian/rules
    - use ocaml.mk cdbs class
  * debian/control
    - bumped ocaml-nox build dependency to >= 3.09.2-7 since we now use
      ocaml.mk

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  4 Nov 2006 09:23:02 +0100

pcre-ocaml (5.11.1-1) unstable; urgency=low

  * New upstream release
  * debian/rules
    - removed no longer needed workaround for cdbs + dpatch
    - avoid to create debian/control from debian/control.in on ocamlinit
  * debian/control.in
    - file removed; it is deprecated as per ocaml packaging policy 3.3
  * debian/patches/
    - commented out 19_meta.dpatch (not needed with this version, lib version
      is correct in upstream's META)

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  5 Sep 2006 21:27:53 +0200

pcre-ocaml (5.10.1-4) unstable; urgency=low

  * Upload to unstable.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 16 May 2006 20:12:18 +0000

pcre-ocaml (5.10.1-3) experimental; urgency=low

  * Rebuilt against OCaml 3.09.2, bumped deps accordingly.
  * Bumped Standards-Version to 3.7.2 (no changes needed).

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 22:09:37 +0000

pcre-ocaml (5.10.1-2) unstable; urgency=low

  * Rebuilt against OCaml 3.09.1, bumped deps accordingly.

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  7 Jan 2006 14:30:33 +0100

pcre-ocaml (5.10.1-1) unstable; urgency=low

  * New upstream release
  * rebuilt with ocaml 3.09
  * debian/*
    - no more hardcoding of ocaml abi anywhere
  * debian/control
    - bumped standards-version
  * debian/patches/19_meta.dpatch
    - bumped lib version in META

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  8 Nov 2005 23:08:44 +0100

pcre-ocaml (5.10.0-3) unstable; urgency=low

  * debian/patches/31_fpic
    - patched OCamlMakefile so that pcre_stubs.o is built with -fPIC,
      fixes FTBFS on amd64 and other archs (Closes: Bug#320624).

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Jul 2005 22:01:15 +0200

pcre-ocaml (5.10.0-2) unstable; urgency=low

  * debian/control
    - added build-dep on dpatch (Closes: Bug#320456).

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 29 Jul 2005 17:36:36 +0200

pcre-ocaml (5.10.0-1) unstable; urgency=low

  * New upstream release
  * debian/*
    - uses CDBS and dpatch

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 25 Jun 2005 09:54:05 +0200

pcre-ocaml (5.08.1-3) unstable; urgency=low

  * Rebuilt against ocaml 3.08.3

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 24 Mar 2005 22:44:54 +0100

pcre-ocaml (5.08.1-2) unstable; urgency=medium

  * debian/control
    - depend on ocaml-base-nox-3.08 instead of ocaml-base-3.08 since
      this package doesn't directly need ocaml X libraries

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 24 Aug 2004 12:18:34 +0200

pcre-ocaml (5.08.1-1) unstable; urgency=low

  * New upstream release
  * Rebuilt with ocaml 3.08
  * debian/control
    - bumped ocaml deps to 3.08
    - bumped standards-version to 3.6.1.1
    - changed ocaml deps to ocaml-nox

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Jul 2004 14:58:03 +0200

pcre-ocaml (5.06.2-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - bumped standards-version to 3.6.1.0
    - bumped dependencies to libpcre3-dev >= 4.5 as needed by this
      upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 15 Mar 2004 19:18:28 +0100

pcre-ocaml (5.03.1-4) unstable; urgency=low

  * Rebuilt with ocaml 3.07

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  1 Oct 2003 13:33:03 +0200

pcre-ocaml (5.03.1-3) unstable; urgency=low

  * Rebuilt with ocaml 3.07beta2

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 22 Sep 2003 17:11:38 +0200

pcre-ocaml (5.03.1-2) unstable; urgency=low

  * bugfix: added build dependencies on ocaml-findlib

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 17 Jun 2003 18:00:31 +0200

pcre-ocaml (5.03.1-1) unstable; urgency=low

  * New upstream release
  * debian/control
    - added versioned dependencies on libcpre3 (>= 4.3)
    - bumped standards-version to 3.5.10
    - removed obsolete Provides libpcre-ocaml-dev-<version>
    - changed section of libpcre-ocaml-dev to libdevel
    - added ${misc:Depends}
  * debian/rules
    - removed DH_COMPAT in favour of debian/compat
    - removed useless NO_CUSTOM=y option when compiling native code
    - use dh_install in place of dh_movefiles

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 17 Jun 2003 13:18:13 +0200

pcre-ocaml (4.30.0-3) unstable; urgency=low

  * Removed Provides:.*-<version>

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 10 Mar 2003 12:50:29 +0100

pcre-ocaml (4.30.0-2) unstable; urgency=low

  * Libdir transition to /usr/lib/ocaml/3.06
  * Changed depends and build depends to ocaml{,-base}-3.06-1

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Dec 2002 12:46:33 +0100

pcre-ocaml (4.30.0-1) unstable; urgency=low

  * New upstream release
  * Create ocamldoc documentation at debian package build time (thus
    removed TODO)
  * Added 'Provides: libpcre-ocaml-dev-<version>' to libpcre-ocaml-dev
  * Bumped Standards-Version to 3.5.8

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 28 Nov 2002 23:04:30 +0100

pcre-ocaml (4.28.2-2) unstable; urgency=low

  * Disabled custom mode forcing, now is possible to build arch:all
    packages against pcre-ocaml
  * Use a more intelligent test to see if ocamlopt is available in
  	debian/rules

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 17 Sep 2002 15:11:27 +0200

pcre-ocaml (4.28.2-1) unstable; urgency=low

  * New upstream release
  * Rebuilt against ocaml 3.06 (Closes: Bug#158248, Bug#158231)
  * Changed deps and build-deps to ocaml-3.06 and ocaml-base-3.06
    accordingly to the new ocaml packaging policy
  * Changed ocaml-findlib from recommends to depends

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Aug 2002 15:50:04 +0200

pcre-ocaml (4.26.3-3) unstable; urgency=low

  * Moved shared objects in /usr/lib/ocaml/stublibs
  * Switched to debhelper 4
  * Commented out dh_ocamlld, no longer needed

 -- Stefano Zacchiroli <zack@debian.org>  Sun,  7 Jul 2002 15:05:56 +0200

pcre-ocaml (4.26.3-2) unstable; urgency=low

  * Bugfix: changed dependency on libpcre3 to libpcre3-dev
  * Added Recommends on ocaml-findlib

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 11 May 2002 11:35:14 +0200

pcre-ocaml (4.26.3-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Mon,  6 May 2002 22:45:22 +0200

pcre-ocaml (4.26.0-1) unstable; urgency=low

  * Initial Release (Closes: Bug#137859).

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  2 May 2002 17:00:14 +0200
